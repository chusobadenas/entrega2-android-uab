package cat.uab.entrega2;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;

import cat.uab.entrega2.adapter.CourseAdapter;
import cat.uab.entrega2.data.UserDataMock;
import cat.uab.entrega2.preferences.PreferencesBundle;

/**
 * Contains courses list.
 */
public class CoursesListActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list);

    // Get data
    UserDataMock userDataMock = UserDataMock.newInstance(this);

    int userId = getIntent().getIntExtra(UserDataMock.USER_ID, 0);
    Bundle bundle = getIntent().getBundleExtra(UserDataMock.USER_PREFS);

    PreferencesBundle preferencesBundle = new PreferencesBundle(bundle);
    ActionBar actionBar = getSupportActionBar();

    if (actionBar != null) {
      // Change title
      String title = getResources().getString(R.string.courses_title) + " " + userDataMock
          .getUserAt(userId).getFullName();
      actionBar.setTitle(title);

      // Enable up navigation
      actionBar.setDisplayHomeAsUpEnabled(true);
    }

    // Create custom adapter
    List<UserDataMock.CourseData> courses = userDataMock.getCoursesForUser(userId);
    CourseAdapter adapter = new CourseAdapter(this, courses, preferencesBundle);

    // Associate adapter
    ListView coursesList = (ListView) findViewById(R.id.list);
    coursesList.setAdapter(adapter);
  }
}
