package cat.uab.entrega2.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cat.uab.entrega2.R;

/**
 * {@link UserDataMock} class for generating test user and course data
 */
public final class UserDataMock {

  public static final String USER_ID = "user_id";
  public static final String USER_PREFS = "user_prefs";

  private static UserDataMock instance;

  /**
   * @param context Android context
   * @return a instance of this class
   */
  public static synchronized UserDataMock newInstance(Context context) {
    if (instance == null) {
      instance = new UserDataMock(context);
    }

    return instance;
  }

  private final Context context;
  private final List<UserData> userDataList;

  /**
   * Private constructor
   *
   * @param context Android context
   */
  private UserDataMock(Context context) {
    this.context = context;

    List<CourseData> courseDataList = initializeCourseData();
    this.userDataList = initializeUserData(courseDataList);
  }

  /**
   * Returns a list of mock users.
   *
   * @return A list of {@link UserData} objects.
   */
  public List<UserData> getUserList() {
    return this.userDataList;
  }

  /**
   * Returns a single {@link UserData} entity.
   *
   * @param position The index of the object in the List.
   * @return A {@link UserData} entity representing a single user.
   */
  public UserData getUserAt(int position) {
    return this.userDataList.get(position);
  }

  /**
   * Returns a list of {@link CourseData} entities for a particular user.
   *
   * @param userId The index or ID of the {@link UserData} object from which we
   *               want to obtain the courses list.
   * @return A collection of {@link CourseData} entities for this particular user.
   */
  public List<CourseData> getCoursesForUser(int userId) {
    return this.userDataList.get(userId).getEnrolledCourses();
  }

  private List<CourseData> initializeCourseData() {
    List<CourseData> courseDataMockList = new ArrayList<>();

    courseDataMockList.add(
        new CourseData(
            "CS 100. Computational Thinking I",
            6,
            "Mon,Tue,Wed,Thu,Fri",
            "10:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 101. Fluency With Information Technology",
            3,
            "Mon,Wed,Thu,Fri",
            "12:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 109. Smart Phone and Wireless Technology",
            3,
            "Mon,Thu,Fri",
            "15:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 199. Special Topcs: Outreach Training",
            6,
            "Mon,Tue,Wed,Thu",
            "09:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 200. Computational Thinking II",
            6,
            "Mon,Wed,Fri",
            "17:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 201. Introduction to Object Oriented Programming",
            9,
            "Tue,Thu",
            "14:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 250. Discrete Structures",
            4,
            "Mon",
            "15:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 302. Object-Oriented Design",
            4,
            "Tue",
            "16:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 303. Algorithms/Data Structures",
            6,
            "Mon,Thu,Fri",
            "08:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 304. Object-Oriented Programming in C++",
            4,
            "Tue,Wed,Fri",
            "10:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 305. Introduction to Python Programming",
            3,
            "Mon,Thu",
            "17:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 330. Computer Organization and Assembly Language Programming",
            3,
            "Tue,Fri",
            "14:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 333. Unix Operating System Fundamentals",
            2,
            "Wed",
            "12:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 334. Internetworking with TCP/IP",
            6,
            "Mon,Thu,Fri",
            "16:00")
    );
    courseDataMockList.add(
        new CourseData(
            "CS 336. Network Security",
            6,
            "Tue,Wed,Fri",
            "18:00")
    );

    return courseDataMockList;
  }

  private List<UserData> initializeUserData(List<CourseData> courseDataMockList) {
    List<UserData> userDataMockList = new ArrayList<>();
    Random randomGenerator = new Random();
    int maxRandNum = courseDataMockList.size();

    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.homer),
            "Homer Simpson",
            "1",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.marge),
            "Marge Simpson",
            "2",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.bart),
            "Bart Simpson",
            "3",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.lisa),
            "Lisa Simpson",
            "4",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.maggie),
            "Maggie Simpson",
            "5",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.aristotle),
            "Aristotle Amadopolis",
            "6",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.wendell),
            "Wendell Borton",
            "7",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.jimbo),
            "Jimbo Jones",
            "8",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.kearney),
            "Kearney Zzyzwicz",
            "9",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );
    userDataMockList.add(
        new UserData(
            BitmapFactory.decodeResource(context.getResources(), R.drawable.kwan),
            "Cookie Kwan",
            "10",
            courseDataMockList.subList(0, randomGenerator.nextInt(maxRandNum)))
    );

    return userDataMockList;
  }

  /**
   * {@link UserData} class for storing user data.
   */
  public static class UserData {

    private final Bitmap picture;
    private final String fullName;
    private final String niu;
    private final List<CourseData> enrolledCourses;

    /**
     * Constructor
     *
     * @param picture
     * @param fullName
     * @param niu
     * @param enrolledCourses
     */
    public UserData(Bitmap picture, String fullName, String niu, List<CourseData> enrolledCourses) {
      this.picture = picture;
      this.fullName = fullName;
      this.niu = niu;
      this.enrolledCourses = new ArrayList<>(enrolledCourses);
    }

    /**
     * @return the picture
     */
    public final Bitmap getPicture() {
      return picture;
    }

    /**
     * @return the fullname
     */
    public final String getFullName() {
      return fullName;
    }

    /**
     * @return the niu
     */
    public final String getNiu() {
      return niu;
    }

    /**
     * @return the enrolled courses
     */
    public final List<CourseData> getEnrolledCourses() {
      return new ArrayList<>(enrolledCourses);
    }

    @Override
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder()
          .append("***** User Data details *****")
          .append("\npicture=")
          .append(this.getPicture())
          .append("\nfullName=")
          .append(this.getFullName())
          .append("\nniu=")
          .append(this.getNiu())
          .append("\nenrolledCourses=")
          .append(this.getEnrolledCourses().size())
          .append("\n*****************************");

      return stringBuilder.toString();
    }
  }

  /**
   * {@link CourseData} class for storing course data.
   */
  public static class CourseData {

    private final String title;
    private final int numberOfCredits;
    private final String daysOfWeek;
    private final String startsAt;

    /**
     * Constructor
     *
     * @param title
     * @param numberOfCredits
     * @param daysOfWeek
     * @param startsAt
     */
    public CourseData(String title, int numberOfCredits, String daysOfWeek, String startsAt) {
      this.title = title;
      this.numberOfCredits = numberOfCredits;
      this.daysOfWeek = daysOfWeek;
      this.startsAt = startsAt;
    }

    /**
     * @return the title
     */
    public final String getTitle() {
      return title;
    }

    /**
     * @return the number of credits
     */
    public final int getNumberOfCredits() {
      return numberOfCredits;
    }

    /**
     * @return the days of week
     */
    public final String getDaysOfWeek() {
      return daysOfWeek;
    }

    /**
     * @return the start hour
     */
    public final String getStartsAt() {
      return startsAt;
    }

    @Override
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder()
          .append("***** Course Data details *****")
          .append("\ntitle=")
          .append(this.getTitle())
          .append("\nnumberOfCredits=")
          .append(this.getNumberOfCredits())
          .append("\ndaysOfWeek=")
          .append(this.getDaysOfWeek())
          .append("\nstartsAt=")
          .append(this.getStartsAt())
          .append("\n*****************************");

      return stringBuilder.toString();
    }
  }
}
