package cat.uab.entrega2.preferences;

import android.os.Bundle;

/**
 * Encapsulates preferences
 */
public class PreferencesBundle {

  public static final String PREF_SHOW_CREDITS = "PREF_SHOW_CREDITS";
  public static final String PREF_SHOW_DAYS = "PREF_SHOW_DAYS";
  public static final String PREF_SHOW_HOUR = "PREF_SHOW_HOUR";

  private final Bundle bundle;

  /**
   * Default constructor
   */
  public PreferencesBundle() {
    bundle = new Bundle();

    bundle.putBoolean(PREF_SHOW_CREDITS, true);
    bundle.putBoolean(PREF_SHOW_DAYS, true);
    bundle.putBoolean(PREF_SHOW_HOUR, true);
  }

  /**
   * Constructor with bundle
   *
   * @param bundle bundle with preferences
   */
  public PreferencesBundle(Bundle bundle) {
    this.bundle = bundle;
  }

  /**
   * Constructor
   *
   * @param showCredits credits must be shown
   * @param showDays    days must be shown
   * @param showHour    hour must be shown
   */
  public PreferencesBundle(boolean showCredits, boolean showDays, boolean showHour) {
    bundle = new Bundle();

    bundle.putBoolean(PREF_SHOW_CREDITS, showCredits);
    bundle.putBoolean(PREF_SHOW_DAYS, showDays);
    bundle.putBoolean(PREF_SHOW_HOUR, showHour);
  }

  /**
   * @return the bundle
   */
  public final Bundle getBundle() {
    return bundle;
  }

  /**
   * @return true if credits must be shown, false otherwise
   */
  public final boolean isCreditsEnabled() {
    return bundle.getBoolean(PREF_SHOW_CREDITS);
  }

  /**
   * @return true if days must be shown, false otherwise
   */
  public final boolean isDaysEnabled() {
    return bundle.getBoolean(PREF_SHOW_DAYS);
  }

  /**
   * @return true if hour must be shown, false otherwise
   */
  public final boolean isHourEnabled() {
    return bundle.getBoolean(PREF_SHOW_HOUR);
  }
}
