package cat.uab.entrega2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import cat.uab.entrega2.adapter.UserAdapter;
import cat.uab.entrega2.data.UserDataMock;
import cat.uab.entrega2.preferences.PreferencesActivity;
import cat.uab.entrega2.preferences.PreferencesBundle;

/**
 * Main activity. Contains users list.
 */
public class UsersListActivity extends AppCompatActivity {

  private static final int PREF_REQ_CODE = 0;

  private PreferencesBundle preferencesBundle = new PreferencesBundle();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list);

    // Change title
    getSupportActionBar().setTitle(R.string.users_title);

    // Update preferences
    updatePreferences();

    // Get data
    UserDataMock userDataMock = UserDataMock.newInstance(this);

    // Create custom adapter
    List<UserDataMock.UserData> users = userDataMock.getUserList();
    UserAdapter adapter = new UserAdapter(this, users);

    // Associate adapter
    ListView usersList = (ListView) findViewById(R.id.list);
    usersList.setAdapter(adapter);

    usersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Navigate to courses
        Intent intent = new Intent(UsersListActivity.this, CoursesListActivity.class);
        intent.putExtra(UserDataMock.USER_ID, position);
        intent.putExtra(UserDataMock.USER_PREFS, preferencesBundle.getBundle());

        startActivity(intent);
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    boolean result = super.onOptionsItemSelected(item);

    // Preferences item
    if (item.getItemId() == R.id.preferences) {
      result = true;
      showPreferences();
    }

    return result;
  }

  private void showPreferences() {
    Intent intent = new Intent(this, PreferencesActivity.class);
    startActivityForResult(intent, PREF_REQ_CODE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    // Preferences code
    if (requestCode == PREF_REQ_CODE) {
      updatePreferences();
    }
  }

  private void updatePreferences() {
    // Get preferences
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
    boolean showCredits = preferences.getBoolean(PreferencesBundle.PREF_SHOW_CREDITS, true);
    boolean showDays = preferences.getBoolean(PreferencesBundle.PREF_SHOW_DAYS, true);
    boolean showHour = preferences.getBoolean(PreferencesBundle.PREF_SHOW_HOUR, true);

    // Update preferences
    preferencesBundle = new PreferencesBundle(showCredits, showDays, showHour);
  }
}
