package cat.uab.entrega2.adapter;

import android.content.Context;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * List adapter
 *
 * @param <T> type of the elements list
 */
public abstract class AbstractListAdapter<T> extends BaseAdapter {

  private final Context context;
  private final List<T> list;

  /**
   * Constructor
   *
   * @param context activity context
   * @param list    list of elements
   */
  public AbstractListAdapter(Context context, List<T> list) {
    super();
    this.context = context;
    this.list = new ArrayList<>(list);
  }

  @Override
  public int getCount() {
    return list.size();
  }

  @Override
  public Object getItem(int position) {
    return list.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  /**
   * @return the context
   */
  public final Context getContext() {
    return context;
  }

  /**
   * @return the list
   */
  public final List<T> getList() {
    return new ArrayList<>(list);
  }
}
