package cat.uab.entrega2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cat.uab.entrega2.R;
import cat.uab.entrega2.data.UserDataMock;

/**
 * User adapter
 */
public class UserAdapter extends AbstractListAdapter<UserDataMock.UserData> {

  /**
   * Constructor
   *
   * @param context activity context
   * @param users   list of users
   */
  public UserAdapter(Context context, List<UserDataMock.UserData> users) {
    super(context, users);
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // Reuse the view of each row. Check always for null to create a new
    // view, otherwise reuse and redraw it!
    View elementView = convertView;

    if (elementView == null) {
      LayoutInflater inflater = LayoutInflater.from(getContext());
      elementView = inflater.inflate(R.layout.user_detail, parent, false);
    }

    // Read user
    UserDataMock.UserData selectedUser = getList().get(position);

    // Get views
    ImageView userImg = (ImageView) elementView.findViewById(R.id.userImage);
    TextView userName = (TextView) elementView.findViewById(R.id.userName);
    TextView userNumOfCourses = (TextView) elementView.findViewById(R.id.userNumOfCourses);

    // Update views
    userImg.setImageBitmap(selectedUser.getPicture());
    userName.setText(selectedUser.getFullName());

    String coursesNum = String.valueOf(selectedUser.getEnrolledCourses().size());
    String coursesWord = getContext().getResources().getString(R.string.courses);
    userNumOfCourses.setText(coursesNum + " " + coursesWord);

    return elementView;
  }
}
