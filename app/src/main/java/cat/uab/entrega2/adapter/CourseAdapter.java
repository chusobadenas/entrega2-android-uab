package cat.uab.entrega2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cat.uab.entrega2.R;
import cat.uab.entrega2.data.UserDataMock;
import cat.uab.entrega2.preferences.PreferencesBundle;

/**
 * Course adapter
 */
public class CourseAdapter extends AbstractListAdapter<UserDataMock.CourseData> {

  private final PreferencesBundle preferencesBundle;

  /**
   * Constructor
   *
   * @param context           activity context
   * @param courses           list of courses
   * @param preferencesBundle preferences
   */
  public CourseAdapter(Context context, List<UserDataMock.CourseData> courses, PreferencesBundle
      preferencesBundle) {
    super(context, courses);
    this.preferencesBundle = preferencesBundle;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // Reuse the view of each row. Check always for null to create a new
    // view, otherwise reuse and redraw it!
    View elementView = convertView;

    if (elementView == null) {
      LayoutInflater inflater = LayoutInflater.from(getContext());
      elementView = inflater.inflate(R.layout.course_detail, parent, false);
    }

    // Read course
    UserDataMock.CourseData selectedCourse = getList().get(position);

    // Get views
    TextView courseName = (TextView) elementView.findViewById(R.id.courseName);
    TextView courseCredits = (TextView) elementView.findViewById(R.id.courseCredits);
    TextView courseDaysOfWeek = (TextView) elementView.findViewById(R.id.courseDays);
    TextView courseHour = (TextView) elementView.findViewById(R.id.courseHour);

    // Update views
    displayValue(courseName, selectedCourse.getTitle(), true);
    displayValue(courseDaysOfWeek, selectedCourse.getDaysOfWeek(), preferencesBundle.isDaysEnabled());
    displayValue(courseHour, selectedCourse.getStartsAt() + " h.", preferencesBundle.isHourEnabled());

    String credits = String.valueOf(selectedCourse.getNumberOfCredits()) + " " + getContext()
        .getResources().getString(R.string.credits);
    displayValue(courseCredits, credits, preferencesBundle.isCreditsEnabled());

    return elementView;
  }

  private void displayValue(TextView view, String value, boolean isShow) {
    // Show value
    if (isShow) {
      view.setVisibility(View.VISIBLE);
    }
    // Hide view
    else {
      view.setVisibility(View.GONE);
    }

    // Update value
    view.setText(value);
  }
}
